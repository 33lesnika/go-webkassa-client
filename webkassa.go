package go_webkassa_client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

type WebkassaAPI interface {
	Authorize(request AuthRequest) (*AuthResponse, error)
	CreateCheck(request CheckRequest) (*CheckResponse, error)
	GetCheckLines(request CheckLinesRequest) (*CheckLinesResponse, error)
	ZReport(request CheckRequest) (*Response, error)
}

var defaultClient, _ = New(ClientConfig{
	ServerURL: "https://kkm.webkassa.kz/api/",
})

func New(config ClientConfig) (WebkassaAPI, error) {
	serverUrl, err := url.Parse(config.ServerURL)
	if err != nil {
		return nil, fmt.Errorf("invalid webkassa http url: %v", err)
	}
	if config.Client == nil {
		config.Client = http.DefaultClient
	}
	return &webkassaHTTPAPI{
		serverURL: serverUrl,
		client:    config.Client,
	}, nil
}

type webkassaHTTPAPI struct {
	serverURL *url.URL
	client    *http.Client
}

var _ WebkassaAPI = (*webkassaHTTPAPI)(nil)

func Authorize(request AuthRequest) (*AuthResponse, error) {
	return defaultClient.Authorize(request)
}

func CreateCheck(request CheckRequest) (*CheckResponse, error) {
	return defaultClient.CreateCheck(request)
}

func ZReport(request CheckRequest) (*Response, error) {
	return defaultClient.ZReport(request)
}

func GetCheckLines(request CheckLinesRequest) (*CheckLinesResponse, error) {
	return defaultClient.GetCheckLines(request)
}

func (wk *webkassaHTTPAPI) Authorize(request AuthRequest) (*AuthResponse, error) {
	response := &AuthResponse{}
	err := wk.postJson(wk.serverURL.JoinPath("Authorize"), request, response)
	if err != nil {
		return nil, fmt.Errorf("cannot authorize to webkassa: %v", err)
	}
	return response, nil
}

func (wk *webkassaHTTPAPI) CreateCheck(request CheckRequest) (*CheckResponse, error) {
	response := &CheckResponse{}
	err := wk.postJson(wk.serverURL.JoinPath("Check"), request, response)
	if err != nil {
		return nil, fmt.Errorf("cannot create check in webkassa: %v", err)
	}
	return response, nil
}

func (wk *webkassaHTTPAPI) GetCheckLines(request CheckLinesRequest) (*CheckLinesResponse, error) {
	response := &CheckLinesResponse{}
	err := wk.postJson(wk.serverURL.JoinPath("Ticket", "PrintFormat"), request, response)
	if err != nil {
		return nil, fmt.Errorf("cannot get check lines from webkassa: %v", err)
	}
	return response, nil
}

func (wk *webkassaHTTPAPI) ZReport(request CheckRequest) (*Response, error) {
	response := &Response{}
	err := wk.postJson(wk.serverURL.JoinPath("Check"), request, response)
	if err != nil {
		return nil, fmt.Errorf("cannot create check in webkassa: %v", err)
	}
	return response, nil
}

func (wk *webkassaHTTPAPI) postJson(url *url.URL, requestBody any, response any) error {
	requestBodyBytes, err := json.Marshal(requestBody)
	if err != nil {
		return fmt.Errorf("cannot json.Marshall http request: %v", err)
	}
	httpResponse, err := http.Post(url.String(), "application/json;charset=utf-8", bytes.NewReader(requestBodyBytes))
	if err != nil {
		return fmt.Errorf("error during http request to webkassa: %v", err)
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(httpResponse.Body)
	bodyBytes, err := io.ReadAll(httpResponse.Body)
	err = json.Unmarshal(bodyBytes, response)
	if err != nil {
		return fmt.Errorf("cannot unmarshall http response body: %v", err)
	}
	return nil
}
