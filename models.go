package go_webkassa_client

import (
	"net/http"
)

type ClientConfig struct {
	ServerURL string
	Client    *http.Client
}
type Response struct {
	Errors []struct {
		Code int    `json:"Code"`
		Text string `json:"Text"`
	} `json:"Errors"`
}

type AuthRequest struct {
	Login    string `json:"Login"`
	Password string `json:"Password"`
}

type AuthResponse struct {
	Response
	Data struct {
		Token string `json:"Token"`
	} `json:"Data"`
}

type CheckRequest struct {
	Token               string     `json:"Token"`
	CashboxUniqueNumber string     `json:"CashboxUniqueNumber"`
	OperationType       int        `json:"OperationType"`
	Positions           []Position `json:"Positions"`
	Payments            []Payment  `json:"Payments"`
	CustomerEmail       string     `json:"CustomerEmail"`
}

type Position struct {
	Count        int    `json:"Count"`
	Price        int    `json:"Price"`
	PositionName string `json:"PositionName"`
	Tax          int    `json:"Tax"`
	TaxPercent   int    `json:"TaxPercent"`
}

type Payment struct {
	Sum         int `json:"sum"`
	PaymentType int `json:"paymentType"`
}

type CheckResponse struct {
	Response
	Data struct {
		CheckNumber        string `json:"CheckNumber"`
		DateTime           string `json:"DateTime"`
		OfflineMode        bool   `json:"OfflineMode"`
		CashboxOfflineMode bool   `json:"CashboxOfflineMode"`
		Cashbox            struct {
			UniqueNumber       string `json:"UniqueNumber"`
			RegistrationNumber string `json:"RegistrationNumber"`
			IdentityNumber     string `json:"IdentityNumber"`
			Address            string `json:"Address"`
			Ofd                struct {
				Name string `json:"Name"`
				Host string `json:"Host"`
				Code int    `json:"Code"`
			} `json:"Ofd"`
		} `json:"Cashbox"`
		CheckOrderNumber int    `json:"CheckOrderNumber"`
		ShiftNumber      int    `json:"ShiftNumber"`
		EmployeeName     string `json:"EmployeeName"`
		TicketUrl        string `json:"TicketUrl"`
		TicketPrintUrl   string `json:"TicketPrintUrl"`
	} `json:"Data"`
}

type ZReportRequest struct {
	Token               string `json:"Token"`
	CashboxUniqueNumber string `json:"CashboxUniqueNumber"`
}

type CheckLinesRequest struct {
	CashboxUniqueNumber string `json:"CashboxUniqueNumber"`
	ExternalCheckNumber string `json:"ExternalCheckNumber"`
	IsDuplicate         string `json:"IsDuplicate"`
	PaperKind           string `json:"PaperKind"`
	Token               string `json:"Token"`
}

type CheckLinesResponse struct {
	Response
	Data struct {
		Lines []struct {
			Order int    `json:"Order"`
			Type  int    `json:"Type"`
			Value string `json:"Value"`
			Style string `json:"Style"`
		} `json:"Lines"`
	} `json:"Data"`
}
